package com.example.bottomnavigationdrawer;

public class ModelFood {

    private int imaqe;
    private String name,place,price;


    public ModelFood(int imaqe, String name, String place, String price) {
        this.imaqe = imaqe;
        this.name = name;
        this.place = place;
        this.price = price;
    }

    public int getImaqe() {
        return imaqe;
    }

    public void setImaqe(int imaqe) {
        this.imaqe = imaqe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
