package com.example.bottomnavigationdrawer;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.example.bottomnavigationdrawer.api.ApiConfig;
import com.example.bottomnavigationdrawer.api.ApiInterface;
import com.example.bottomnavigationdrawer.model.ResponseResturant;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    ArrayList<ModelFood> foodList;
    private ApiInterface retrofitInterface;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.rv_food);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL,false));

        foodList = new ArrayList<>();
        foodList.add(new ModelFood(R.drawable.slide3, "biriyani", "Dhaka", "4"));
        foodList.add(new ModelFood(R.drawable.slide3, "halem", "Dhaka", "4"));
        foodList.add(new ModelFood(R.drawable.slide3, "chaat", "Dhaka", "4"));
        foodList.add(new ModelFood(R.drawable.slide3, "kare", "Dhaka", "4"));

        FoodAdapter foodAdapter = new FoodAdapter(getActivity(),foodList);
        recyclerView.setAdapter(foodAdapter);
        foodAdapter.notifyDataSetChanged();

        //creting instance of Interface by making a call to create method on retrofit Instance
        retrofitInterface = ApiConfig.getRetrofitInstance().create(ApiInterface.class);
        //making the API call
        Call<ResponseResturant> call = retrofitInterface.getResturants();

        call.enqueue(new Callback<ResponseResturant>() {
            @Override
            public void onResponse(Call<ResponseResturant> call, Response<ResponseResturant> response) {
                //TODO things to do in successful response
            }

            @Override
            public void onFailure(Call<ResponseResturant> call, Throwable t) {
                //TODO response is not successful
            }
        });

        return view;


//        gallery = (Gallery) container.findViewById(R.drawable.slide1);
//        gallery.setAdapter(adapter);
//        super.onViewCreated(view, savedInstanceState);


        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_home, container, false);
    }






}
