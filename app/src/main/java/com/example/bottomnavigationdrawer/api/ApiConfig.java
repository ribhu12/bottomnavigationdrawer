package com.example.bottomnavigationdrawer.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiConfig {

    private static Retrofit retrofit = null;
    public static final String BASE_URL = "http://services.hanselandpetal.com/";

    public static Retrofit getRetrofitInstance(){

        if (retrofit==null){

            retrofit =new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
