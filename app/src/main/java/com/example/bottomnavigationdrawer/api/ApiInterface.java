package com.example.bottomnavigationdrawer.api;

import com.example.bottomnavigationdrawer.model.ResponseResturant;

import retrofit2.Call;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("endpoint")
    Call<ResponseResturant> getResturants();
}
